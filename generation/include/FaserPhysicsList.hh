#ifndef FaserPhysicsList_hh
#define FaserPhysicsList_hh 1

#include "G4VModularPhysicsList.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"

#include "Newaxion1.hh"
class FaserPhysicsMessenger;

class FaserPhysicsList : public G4VModularPhysicsList
{
public:
  FaserPhysicsList();
  virtual ~FaserPhysicsList();

  virtual void ConstructParticle();

  virtual void SetCuts();

  virtual void ConstructProcess();

  void setDarkPhotonMass(G4double mass) { if (Newaxion1::axion1() == nullptr) fDarkPhotonMass = mass; }
  G4double getDarkPhotonMass() { return (Newaxion1::axion1() != nullptr ? Newaxion1::axion1()->GetPDGMass() : fDarkPhotonMass); }

  static constexpr G4double default_darkPhoton_mass = 100.0 * MeV;

private:
  G4double fDarkPhotonMass;
  FaserPhysicsMessenger* fPhysicsMessenger;

};
#endif
